<!-- This file was written by Bing Ai. :) -->

# win_after_format

<p>A project that automates the installation of Windows applications after formatting.</p>

## Features

<p>Installs a list of applications using winget, a Windows package manager.<br>
Applies a registry file (settinge.reg) to customize Windows settings.</p>

## Usage

<p>Download or clone this repository to your computer.<br>
Edit the winget_apps_list.txt file to add or remove the applications you want to install.<br>
run main.bat as administrator to start the installation process.<br>
Enjoy your fresh Windows system with your favorite applications :).<br></p>
