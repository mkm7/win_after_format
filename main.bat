@REM downloading and installing apps using winget
@REM run in powershell as administrator

for /f %%i in (winget_apps_list.txt) do winget install %%i --accept-source-agreements --accept-package-agreements

@REM copy files to there location
copy .\streamdeck\Default_Profile.streamDeckProfile C:\Users\%username%\Desktop
copy .\streamdeck\Sound-Shortcut.lnk C:\Users\%username%\Documents
copy .\streamdeck\Network_Connections-Shortcut.lnk C:\Users\%username%\Documents
mkdir "C:\Users\%username%\Documents\braveSyncer\"
copy .\braveBookmarksSyncer.bat C:\Users\%username%\Documents\braveSyncer\
copy ".\orca slicer.bat" "C:\Users\%username%\Documents\orca slicer"
auditpol /set /subcategory:"other logon/logoff events" /success:enable /failure:disable
SCHTASKS /CREATE /TN "orca_sync" /SC ONEVENT /MO "*[System[(EventID=4801)]]" /EC Security /TR "C:\Users\%username%\Documents\orca_slicer\orca_slicer.bat"
SCHTASKS /CREATE /TN "braveSyncer" /SC ONEVENT /MO "*[System[(EventID=4801)]]" /EC Security /TR "C:\Users\%username%\Documents\braveSyncer\braveBookmarksSyncer.bat"


@REM scan windows for any missing files
sfc /scannow

setting.reg

echo please restart your device to apply the changes ...
pause