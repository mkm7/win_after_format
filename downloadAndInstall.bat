cd C:\Users\%username%\Desktop
mkdir "auto install"
cd "auto install"
@REM downlaod some apps
curl --location -o "Git-2.45.2-64-bit.exe" "https://github.com/git-for-windows/git/releases/download/v2.45.2.windows.1/Git-2.45.2-64-bit.exe" 
curl --location -o "python-3.12.4-amd64.exe" "https://www.python.org/ftp/python/3.12.4/python-3.12.4-amd64.exe" 
curl --location -o "vscode.exe" "https://code.visualstudio.com/sha/download?build=stable&os=win32-x64-user" 
curl --location -o "modrinth.msi" "https://launcher-files.modrinth.com/versions/0.7.1/windows/Modrinth%%20App_0.7.1_x64_en-US.msi" 
curl --location -o "Fusion Client Downloader.exe" "https://dl.appstreaming.autodesk.com/production/installers/Fusion%%20Client%%20Downloader.exe" 
curl --location -o "LM-Studio-0.2.31-Setup.exe" "https://releases.lmstudio.ai/windows/0.2.31/candidate/b/LM-Studio-0.2.31-Setup.exe" 
curl --location -o "BraveBrowserSetup-BRV010.exe" "https://github.com/brave/brave-browser/releases/download/v1.68.141/BraveBrowserSetup.exe" 
curl --location -o "arduino-ide_2.3.2_Windows_64bit.msi" "https://downloads.arduino.cc/arduino-ide/arduino-ide_2.3.2_Windows_64bit.msi" 
curl --location -o "OrcaSlicer_Windows_Installer_V2.1.1.exe" "https://github.com/SoftFever/OrcaSlicer/releases/download/v2.1.1/OrcaSlicer_Windows_Installer_V2.1.1.exe" 
curl --location -o "Nextcloud-3.13.2-x64.msi" "https://github.com/nextcloud-releases/desktop/releases/download/v3.13.2/Nextcloud-3.13.2-x64.msi" 
curl --location -o "UbisoftConnectInstaller.exe" "https://static3.cdn.ubi.com/orbit/launcher_installer/UbisoftConnectInstaller.exe"  
curl --location -o "jdk-22_windows-x64_bin.msi" "https://download.oracle.com/java/22/latest/jdk-22_windows-x64_bin.msi" 
curl --location -o "ideaIC-2024.2.0.1.exe" "https://download.jetbrains.com/idea/ideaIC-2024.2.0.1.exe"
curl --location -o "kicad-8.0.8-arm64.exe" "https://github.com/KiCad/kicad-source-mirror/releases/download/8.0.8/kicad-8.0.8-arm64.exe"
curl --location -o "win64-ugs-platform-app-2.1.9.zip" "https://github.com/winder/Universal-G-Code-Sender/releases/download/v2.1.9/win64-ugs-platform-app-2.1.9.zip"
curl --location -o "SteamSetup.exe" "https://cdn.fastly.steamstatic.com/client/installer/SteamSetup.exe"
curl --location -o "EpicInstaller-17.2.0.msi?launcherfilename=EpicInstaller-17.2.0-a7917037d5f74d5ca7a0e063d628a662.msi" "https://epicgames-download1.akamaized.net/Builds/UnrealEngineLauncher/Installers/Win32/EpicInstaller-17.2.0.msi?launcherfilename=EpicInstaller-17.2.0-a7917037d5f74d5ca7a0e063d628a662.msi"
curl --location -o "bisoftConnectInstaller.exe" "https://static3.cdn.ubi.com/orbit/launcher_installer/UbisoftConnectInstaller.exe"
curl --location -o "EAappInstaller.exe" "https://origin-a.akamaihd.net/EA-Desktop-Client-Download/installer-releases/EAappInstaller.exe"
]







move win64-ugs-platform-app-2.1.9.zip C:\Users\%username%\Desktop
@REM install some apps


"BraveBrowserSetup-BRV010.exe" /S 
"python-3.12.4-amd64.exe" /quiet InstallAllUsers=1 PrependPath=1 Include_debug=1 Include_symbols=1 
"Fusion Client Downloader.exe" /S 
"LM-Studio-0.2.31-Setup.exe" /S 
"OrcaSlicer_Windows_Installer_V2.1.1.exe" /S 
"UbisoftConnectInstaller.exe" /S 
"ideaIC-2024.2.0.1.exe" /S
"kicad-8.0.8-arm64.exe" /S
"SteamSetup.exe" /S
"bisoftConnectInstaller.exe" /S
"EAappInstaller.exe" /S

msiexec /i "modrinth.msi" /quiet 
msiexec /i "arduino-ide_2.3.2_Windows_64bit.msi" /quiet 
msiexec /i "Nextcloud-3.13.2-x64.msi" /quiet  
msiexec /i "jdk-22_windows-x64_bin.msi" /quiet 
msiexec /i "EpicInstaller-17.2.0.msi?launcherfilename=EpicInstaller-17.2.0-a7917037d5f74d5ca7a0e063d628a662.msi" /quiet

